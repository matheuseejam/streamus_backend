<?php
namespace Sistema;

use Persistencia\PersistenciaMySQL;
use Persistencia\PersistenciaMongoDB;
use InterfacePersistencia\InterfacePersistencia;

class Sistema {
  private $persistencia;

  public function __construct() {
    try {
      $this->persistencia = new PersistenciaMongoDB();
    }
    catch(PersistenciaException $e) {
      throw new Exception('Falha ao obter a conexão com o banco de dados', 0, $e);
    }
  }

  public function getVOD($id, $removerMomentos = false)  {
    if(!$this->checarStringEntrada($id)) return null;
    try {

      $r = $this->persistencia->getVOD ((String)$id);
      if($removerMomentos && array_key_exists('linhaDoTempo', $r))
        unset($r['linhaDoTempo']);
      return $r;

    }
    catch(PersistenciaException $e) {
      return null;
    }
  }
  public function getGame($id) {
    if(!$this->checarStringEntrada($id)) return null;
    try{
      return $this->persistencia->getGame((String)$id);
    }
    catch(PersistenciaException $e) {
      return null;
    }
  }
  public function getUser($id) {
    if(!$this->checarStringEntrada($id)) return null;
    try {
      return $this->persistencia->getUser((String)$id);
    }
    catch(PersistenciaException $e) {
      return null;
    }
  }
  public function getUserByName($nome) {
    if(!$this->checarStringEntrada($nome)) return null;

    try {
      return $this->persistencia->getUserByName((String)$nome);
    }
    catch(PersistenciaException $e) {
      return null;
    }
  }

  public function searchUser($nome) {
    if(strlen($nome) < 4 || !$this->checarStringEntrada($nome)) return [];

    try {
      return $this->persistencia->searchUser((String)$nome);
    }
    catch(PersistenciaException $e) {
      return [];
    }
  }

  public function updateUser($user) {
    if($user == null) return false;
    if(!$this->checarImgInvalida($user['profile_image_url']) && !$this->checarImgInvalida($user['offline_image_url'])) return false;

    try {
      return $this->persistencia->updateUser($user['id']);
    }
    catch(PersistenciaException $e) {
      return false;
    }
  }

  public function getVODsDoStreamer($id, $inicio, $fim, $manterMomentos = false) {
    if(!$this->menorQue8Dias((int)$inicio, (int)$fim)) return null;

    try {
      $vods = $this->persistencia->getVODsDoStreamerPorPeriodo((string) $id, (int) $inicio, (int) $fim);
    }
    catch(PersistenciaException $e) {return null;}

    if($vods !== null && !$manterMomentos)
      for($x = 0; $x < count($vods); $x++)
        unset($vods[$x]['linhaDoTempo']);

    return $vods;
  }

  public function getVODsDoStreamerPorPagina($id, $pagina, $manterMomentos = false) {
    $pagina = (int) $pagina;
    if($pagina < 0) $pagina = 0;

    try {
      $vods = $this->persistencia->getVODsDoStreamerPorPagina((string) $id, $pagina);
    }
    catch(PersistenciaException $e) {return null;}

    if($vods !== null && !$manterMomentos)
      for($x = 0; $x < count($vods); $x++)
        unset($vods[$x]['linhaDoTempo']);

    return $vods;
  }

  public function getVODsDoPeriodo($inicio, $fim, $manterMomentos = false) {
    if(!$this->menorQue8Dias((int)$inicio, (int)$fim)) return null;

    try {
      $vods = $this->persistencia->getVODsDoPeriodo((int) $inicio, (int) $fim);
    }
    catch(PersistenciaException $e) {return null;}

    if($vods !== null && !$manterMomentos)
      for($x = 0; $x < count($vods); $x++)
        unset($vods[$x]['linhaDoTempo']);

    return $vods;
  }

  public function streamersEmAscensao() {
    $cache = $this->getCache('streamersEmAscensao');
    if($cache !== false) return $cache;

    try {
      $vods = $this->persistencia->getVODsDoPeriodo(time()-(60*60*24*7), time());
    }
    catch(PersistenciaException $e) {return null;}

    $lista = [];
    foreach ($vods as $chave => $valor) {
      $vods[$chave]['media'] = $this->getMediaViewsFromMomentos($valor['linhaDoTempo']);

      if(array_key_exists($valor['user_id'], $lista))
        array_push($lista[$valor['user_id']], $vods[$chave]['media']);
      else
        $lista[$valor['user_id']] = [ $vods[$chave]['media'] ];

    }

    $retorno = [];
    foreach ($lista as $chave => $valor)
      if(count($valor) > 2)
        array_push($retorno, ['idUser' => $chave, 'medias' => $valor]);

    $this->ordenarStreamersEmAscensao($retorno);

    for($x = 0; $x < count($retorno); $x++) {
      $streamer = $this->getUser($retorno[$x]['idUser']);

      if($streamer !== null) {
        $retorno[$x] = [
                        'nome' => $streamer['display_name'],
                        'imagem' => $streamer['profile_image_url'],
                        'Viewers' => $retorno[$x]['medias']
                      ];
      }
      else {
        $retorno[$x] = [
                        'nome' => 'Não Registrado',
                        'imagem' => 'https://static-cdn.jtvnw.net/jtv-static/404_preview-300x300.png',
                        'Viewers' => $retorno[$x]['medias']
                      ];
          }
    }

    $this->setCache('streamersEmAscensao', $retorno, 60*30);

    return $retorno;
  }

  private function ordenarStreamersEmAscensao(&$lista) {
    usort($lista, function($a, $b)
                          {
                            $somatorioA = 0;
                            for($x = 0; $x < count($a['medias'])-1; $x++)
                              if($a['medias'][$x] != 0)
                                $somatorioA += ($a['medias'][$x+1] - $a['medias'][$x]) / $a['medias'][$x];
                            $mediaA = $somatorioA/count($a['medias']);

                            $somatorioB = 0;
                            for($x = 0; $x < count($b['medias'])-1; $x++)
                              if($b['medias'][$x] != 0)
                                $somatorioB += ($b['medias'][$x+1] - $b['medias'][$x]) / $b['medias'][$x];
                            $mediaB = $somatorioB/count($b['medias']);

                            if ($mediaA == $mediaB) return 0;
                            return ($mediaA > $mediaB) ? -1 : 1;
                          }
          );
  }

  public function maisJogadosSemana() {
    $cache = $this->getCache('maisJogados');
    if($cache !== false) return $cache;

    try {
      $vods = $this->persistencia->getVODsDoPeriodo(time()-(60*60*24*7), time());
    }
    catch(PersistenciaException $e) {return null;}

    if($vods == null) $vods = [];

    $maisJogados = [];
    foreach ($vods as $chave => $valor) {
      $games = $this->getGamesFromMomentos($valor['linhaDoTempo']);

      for($x = 0; $x < count($games); $x++) {
        if(array_key_exists($games[$x], $maisJogados))
          $maisJogados[$games[$x]] += 1;
        else
          $maisJogados[$games[$x]] = 1;
      }
    }
    uasort($maisJogados, function($a, $b)
                          {
                            if ($a == $b) return 0;
                            return ($a > $b) ? -1 : 1;
                          }
    );
    $vetor = [];
    foreach ($maisJogados as $chave => $valor) {
      $jogo = $this->getGame($chave);

      $imagem = 'https://static-cdn.jtvnw.net/ttv-static/404_boxart-{width}x{height}.jpg';
      $nome = 'Não Registrado';

      if($jogo !== null) {
        $imagem = $jogo['box_art_url'];
        $nome = $jogo['name'];
      }
      array_push($vetor, array('idJogo' => $chave, 'nome' => $nome, 'nLives' => $valor, 'imagem' => $imagem));
    }

    $this->setCache('maisJogados', $vetor, 60*30);

    return $vetor;
  }

  private function setCache($nome, $data, $tempo = (60*10)) {
    $arquivo = 'systemCache_'.$nome.'.json';

    if(file_exists($arquivo)) {
      $temp = json_decode(file_get_contents($arquivo), 1);
      if(time() > $temp['expiracao']) @unlink($arquivo);
      else return True;
    }

    $json = json_encode(['expiracao' => time()+$tempo, 'data' => $data]);
    file_put_contents($arquivo, $json);

    return True;
  }

  private function getCache($nome) {
    $arquivo = 'systemCache_'.$nome.'.json';
    if(file_exists($arquivo)) {
      $temp = json_decode(file_get_contents($arquivo), 1);
      if(time() > $temp['expiracao']) {
        @unlink($arquivo);
        return false;
      }
      return $temp['data'];
    }
    return false;
  }

  private function getGamesFromMomentos($momentos) {
    $games = [];
    foreach ($momentos as $chave => $valor) {
      if(!in_array($valor['game_id'], $games))
        array_push($games, $valor['game_id']);
    }
    return $games;
  }

  private function getMediaViewsFromMomentos($momentos) {
    if(count($momentos) <= 0) return 0;

    $total = 0;
    foreach ($momentos as $chave => $valor)
      $total += $valor['viewer_count'];

    return (int)($total/count($momentos));
  }

  private function menorQue8Dias(int $inicio, int $fim): bool {return ( ($fim-$inicio) < (60*60*24*8) );}

  public function getStreamersOnline() {
    $cache = $this->getCache('streamersOnline');
    if($cache !== false) return $cache;

    $ch = curl_init("https://api.twitch.tv/helix/streams?first=100");
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Client-ID: '.InterfacePersistencia::Twitch_ClientID]);
    curl_setopt($ch , CURLOPT_RETURNTRANSFER, true );

    $r = json_decode(curl_exec($ch), 1);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if($httpcode !== 200) return []; //Erro

    $vetor = [];
    for($x = 0; $x < 10; $x++) {

      foreach($r['data'] as $chave => $valor) {
        if($valor['language'] == 'pt') {

          foreach ($vetor as $chaveInterna => $valorInterno)
            if($valorInterno['nome'] == $valor['user_name'])
              continue;

          array_push($vetor, ['nome' => $valor['user_name'], 'nViews' => $valor['viewer_count']]);

        }
      }

      $ch = curl_init("https://api.twitch.tv/helix/streams?first=100&after=".$r['pagination']['cursor']);
      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Client-ID: '.InterfacePersistencia::Twitch_ClientID]);
      curl_setopt($ch , CURLOPT_RETURNTRANSFER, true );
      $r = json_decode(curl_exec($ch), 1);
      if(curl_getinfo($ch, CURLINFO_HTTP_CODE) !== 200) break;
    }
    curl_close($ch);

    $limite = count($vetor);
    if($limite > 10) $limite = 10;
    $final = [];
    for($x = 0; $x < $limite; $x++) {

      $ch = curl_init("https://api.twitch.tv/helix/users?login=".$vetor[$x]['nome']);
      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Client-ID: '.InterfacePersistencia::Twitch_ClientID]);
      curl_setopt($ch , CURLOPT_RETURNTRANSFER, true );
      $r = json_decode(curl_exec($ch), 1);
      $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      if($httpcode === 200 )
        array_push($final, ['nome' => $r['data'][0]['display_name'],
                            'imagem' => $r['data'][0]['profile_image_url'],
                            'nViews' => $vetor[$x]['nViews']
                            ]
                  );

    }

    $this->setCache('streamersOnline', $final, 60*5);

    return $final;
  }

  public function checarImgInvalida($url) {
    $ch = curl_init($url);
    curl_setopt($ch , CURLOPT_RETURNTRANSFER, true );
    $r = curl_exec($ch);

    if(curl_getinfo($ch)['redirect_url'] === 'https://static-cdn.jtvnw.net/jtv-static/404_preview-300x300.png' ||
       curl_getinfo($ch)['redirect_url'] === 'https://static-cdn.jtvnw.net/jtv-static/404_preview-1920x1080.png')
      return true;

    return false;
  }

  public function getGlobalTwitchBadges() {
    $cache = $this->getCache('globalTwitchBadges');
    if($cache !== false) return $cache;

    $ch = curl_init('https://badges.twitch.tv/v1/badges/global/display');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $r = curl_exec($ch);

    if(curl_getinfo($ch)['http_code'] === 200) {
      $this->setCache('globalTwitchBadges', $r, 60*30);
      return $r;
    }

    return null;
  }

  public function getChannelTwitchBadges($cod) {
    $ch = curl_init('https://badges.twitch.tv/v1/badges/channels/'.$cod.'/display');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $r = curl_exec($ch);

    if(curl_getinfo($ch)['http_code'] === 200)
      return $r;

    return null;
  }

  private function checarStringEntrada(string $s) {
    mb_internal_encoding( 'utf-8' );
    return \mb_eregi ('^([a-z]|[0-9]|(ç|é|á|ó|ú|í|ã|õ|â|ê|î|ô|û|à|è|ì|ò|ù|_)){1,150}$', utf8_encode($s));
  }

}

?>
