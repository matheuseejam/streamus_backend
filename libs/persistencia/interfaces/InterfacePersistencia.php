<?php
namespace InterfacePersistencia;

interface InterfacePersistencia {

  const Mongo_URL = 'mongodb://matheus:hakunamatataeita@twitchus-shard-00-00-6x2nm.mongodb.net:27017,twitchus-shard-00-01-6x2nm.mongodb.net:27017,twitchus-shard-00-02-6x2nm.mongodb.net:27017/test?ssl=true&replicaSet=TwitchUs-shard-0&authSource=admin&retryWrites=true';
  const Mongo_VodsPorVez = 10; // Retorne 10 VODs de um canal por vez
  const Twitch_ClientID = 'xkiy482gril7xrtqjl779irqt05cfs';

  /*
    Esta função retorna um VOD(Video On Demand) especifico do banco de dados
    id -- O id do vod a ser buscado no banco

    Retorna um vetor associativo contendo os dados do VOD solicitado, ou, NULL caso não encontre
  */
  public function getVOD(string $id);

  /*
    Esta função retorna um Streamer especifico do banco de dados
    id -- O id do streamer a ser buscado no banco

    Retorna um vetor associativo contendo os dados do streamer solicitado, ou, NULL caso não encontre
  */
  public function getUser(string $id);

  /*
    Esta função retorna um Streamer especifico do banco de dados
    nome -- O nome de usuário do streamer a ser buscado no banco

    Retorna um vetor associativo contendo os dados do streamer solicitado, ou, NULL caso não encontre
  */
  public function getUserByName(string $nome);

  /*
    Esta função retorna um Jogo especifico do banco de dados
    id -- O id do jogo a ser buscado no banco

    Retorna um vetor associativo contendo os dados do jogo solicitado, ou, NULL caso não encontre
  */
  public function getGame(string $id);

  /*
    Esta função retorna uma lista de VODs que foram criados entre inicio, fim
    inicio -- O momento de inicio da busca (Timestamp)
    fim -- O momento de fim da busca (Timestamp)

    Retorna um vetor de vetores associativos contendo os dados dos VODs solicitados, ou, [] caso não encontre nada
  */
  public function getVODsDoPeriodo(int $inicio, int $fim);

  /*
    Esta função retorna os VODs de um streamer dentro do período solicitado
    idStreamer -- O id do streamer que se deseja obter os vods
    inicio -- O momento de inicio da busca (Timestamp)
    fim -- O momento de fim da busca (Timestamp)

    Retorna um vetor de vetores associativos contendo os dados dos VODs solicitados, ou, [] caso não encontre nada
  */
  public function getVODsDoStreamerPorPeriodo(string $idStreamer, int $inicio, int $fim);

  /*
    Esta função retorna os VODs de um streamer dada uma página
    idStreamer -- O id do streamer que se deseja obter os vods
    pagina -- A página(int) desejada

    Retorna um vetor de vetores associativos contendo os dados dos VODs solicitados, ou, [] caso não encontre nada
  */
  public function getVODsDoStreamerPorPagina(string $idStreamer, int $pagina);

  /*
    Esta função busca pelo dome de um usuário no banco
    nome -- O nome ou parte do nome do usuário

    Retorna um vetor de strings de nomes encontrados
  */
  public function searchUser(string $nome);

  /*
    Esta função atualiza as informações de um streamer diretamente da API da Twitch
    idStreamer -- O id do streamer que se deseja atualizar

    Retorna true caso sucesso e false em caso de falha
  */
  public function updateUser(string $idStreamer);
}

?>
