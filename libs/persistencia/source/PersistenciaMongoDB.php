<?php
namespace Persistencia;

use InterfacePersistencia\InterfacePersistencia;

class PersistenciaMongoDB implements InterfacePersistencia {
  private $con; // A conexão com o banco, Instancia da classe PDO

	function __construct() {
    try {
      $this->con = new \MongoDB\Client( self::Mongo_URL );
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }
	}

  public function getVOD(string $id) {
    $vods = $this->con->TwitchUs->VOD;
    try {
      $resultado = $vods->find(['id' => $id]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $vod = null;
    if($resultado != null)
      foreach ($resultado as $i) {
        $vod = $i;
        break;
      }

    if($vod !== null) {
      unset($vod['_id']);
    }

    return $vod;
  }

  public function getUser(string $id) {
    $users = $this->con->TwitchUs->users;

    try {
      $resultado = $users->find(['id' => $id]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $user = null;
    if($resultado != null)
      foreach ($resultado as $i) {
        $user = $i;
        break;
      }

    if($user !== null) {
      unset($user['_id']);
    }

    return $user;
  }

  public function getUserByName(string $nome) {
    $users = $this->con->TwitchUs->users;

    try {
      $resultado = $users->find(['login' => $nome]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $user = null;
    if($resultado != null)
      foreach ($resultado as $i) {
        $user = $i;
        break;
      }

    if($user !== null) {
      unset($user['_id']);
    }

    return $user;
  }

  public function getGame(string $id) {
    $jogos = $this->con->TwitchUs->jogos;

    try {
      $resultado = $jogos->find(['id' => $id]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $jogo = null;
    if($resultado != null)
      foreach ($resultado as $i) {
        $jogo = $i;
        break;
      }

    if($jogo !== null) {
      unset($jogo['_id']);
    }

    return $jogo;
  }

  public function searchUser(string $nome) {
    $users = $this->con->TwitchUs->users;
    $regex = '.*'.$nome.'.*';
    try {
      $resultado = $users->find(['display_name' => [
                                                    '$regex' => $regex,
                                                    '$options' => 'i'
                                                    ]
                                ], ['limit' => 5]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $final = [];
    if($resultado != null)
      foreach ($resultado as $i) {
        array_push($final, $i['display_name']);
      }

    return $final;
  }

  public function getVODsDoPeriodo(int $inicio, int $fim) {
    $vods = $this->con->TwitchUs->VOD;

    try {
      $resultado = $vods->find(['started_at' => ['$gt' => $inicio, '$lt' => $fim]]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $final = [];
    if($resultado != null)
      foreach ($resultado as $i) {
        unset($i['_id']);
        array_push($final, $i);
      }

    return $final;
  }

  public function getVODsDoStreamerPorPeriodo(string $idStreamer, int $inicio, int $fim) {
    $vods = $this->con->TwitchUs->VOD;

    try {
      $resultado = $vods->find(['user_id' => $idStreamer, 'started_at' => ['$gt' => $inicio, '$lt' => $fim]]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $final = [];
    if($resultado != null)
      foreach ($resultado as $i) {
        unset($i['_id']);
        array_push($final, $i);
      }

    return $final;
  }

  public function getVODsDoStreamerPorPagina(string $idStreamer, int $pagina) {
    $vods = $this->con->TwitchUs->VOD;
    $skip = $pagina * self::Mongo_VodsPorVez;
    if($skip  < 0) $skip = 0;

    try {
      $resultado = $vods->find(['user_id' => $idStreamer], ['skip' => $skip, 'sort' => ['started_at' => -1], 'limit' => self::Mongo_VodsPorVez]);
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    $final = [];
    if($resultado != null)
      foreach ($resultado as $i) {
        unset($i['_id']);
        array_push($final, $i);
      }

    return $final;
  }

  public function updateUser(string $idStreamer) {
    $novo = $this->getInfoUser($idStreamer);
    if($novo === null) return false;

    $users = $this->con->TwitchUs->users;

    try {
      $resultado = $users->updateOne(
                      [ 'id' => $novo['id'] ],
                      [ '$set' => $novo]
                  );
    }
    catch(\Exception $e) {
      throw new PersistenciaException('Falha na conexão com o banco de dados');
    }

    return true;
  }

  private function getInfoUser(string $idStreamer) {
      $ch = curl_init("https://api.twitch.tv/helix/users?id=".$idStreamer);
      curl_setopt($ch, CURLOPT_HTTPHEADER, ['Client-ID: '.self::Twitch_ClientID]);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
      $r = json_decode(curl_exec($ch), 1);

      if($r['data'] !== null)
        return $r['data'][0];
      else
        return null;
  }

}

?>
