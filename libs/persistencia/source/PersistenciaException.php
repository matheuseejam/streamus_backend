<?php
namespace Persistencia;

use Exception;

class PersistenciaException extends Exception {
	protected $estado;

	public function addEstado(string $nome, $valor) {
		if($this->estado == null) $this->estado = "";
		$this->estado .= $nome . ": " . (string)$valor . "\n";
	}
	public function getEstado(): string {
		if($this->estado == null)
			return 'Estado não definido';
		return $this->estado;
	}

}
