<?php
require_once('vendor/autoload.php');

use Sistema\Sistema;
$sistema = new Sistema();

header('Content-Type: application/json');
header('Access-Control-Allow-Origin: *');

set_time_limit(0);

// Obtendo a URI
$URI = mb_substr($_SERVER['REQUEST_URI'], 1);
// Removendo os parâmetros que podem ter sido passados
$URI = explode('?', $URI)[0];
// Separando o caminho em um vetor
$URI = explode('/', $URI);

// Checando o verbo HTTP usado
switch ($_SERVER['REQUEST_METHOD']) {
  case 'GET':
    executarGET($URI);
    break;
  case 'PATCH':
    executarUPDATE($URI);
    break;
  case 'OPTIONS':
    executarOPTIONS($URI);
    break;

  default:
    header('HTTP/1.1 400 Bad Request', true, 400);
    echo '{"erro": "Verbo HTTP não suportado"}';
    break;
}


/*
  ZONA DE FUNÇÕES
*/

function executarOPTIONS($URI) {
  header('Allow: OPTIONS, GET, PATCH, POST', true, 200);
  header('Access-Control-Allow-Methods: OPTIONS, GET, PATCH, POST', true, 200);
}

function executarGET($URI) {
  switch ($URI[0]) {
    case 'vod':
      get_VOD($URI);
      break;
    case 'user':
      get_USER($URI);
      break;
    case 'userByName':
      get_USERByName($URI);
      break;
    case 'jogo':
      get_JOGO($URI);
      break;
    case 'vods':
      get_VODS($URI);
      break;
    case 'estatisticas':
      get_Estatisticas($URI);
      break;
    case 'globalTwitchBadges':
      get_GlobalTwitchBadges();
      break;
    case 'channelTwitchBadges':
      get_ChannelTwitchBadges($URI);
      break;
    case 'searchUser':
      get_SearchUser($URI);
      break;
    default:
      echo '{"erro": "Comando não reconhecido"}';
      break;
  }
}

function executarUPDATE($URI) {
  switch ($URI[0]) {
    case 'user':
      update_user($URI);
      break;
    case 'userByName':
      update_userByName($URI);
      break;

    default:
      echo '{"erro": "Comando não reconhecido"}';
      break;
  }
}

/*
VERBO: PATCH
ENDPOINT: /user
*/
function update_user($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 404 Not Found', true, 404);
    echo '{"erro": "Informe o ID do USER"}';
    return false;
  }

  $user = $sistema->getUser($URI[1]);

  if($user == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  if(!$sistema->updateUser($user))
    header('HTTP/1.1 400 Bad Request', true, 400);

  return true;
}

/*
VERBO: PATCH
ENDPOINT: /userByName
*/
function update_userByName($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 404 Not Found', true, 404);
    echo '{"erro": "Informe o ID do USER"}';
    return false;
  }

  $user = $sistema->getUserByName($URI[1]);

  if($user == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  if(!$sistema->updateUser($user))
    header('HTTP/1.1 400 Bad Request', true, 400);

  return true;
}

/*
VERBO: GET
ENDPOINT: /vod
*/
function get_VOD($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 404 Not Found', true, 404);
    echo '{"erro": "Informe o ID do VOD"}';
    return false;
  }

  $vod = $sistema->getVOD($URI[1], array_key_exists("removerMomentos", $_GET));

  if($vod == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  echo json_encode($vod);

  return true;
}

/*
VERBO: GET
ENDPOINT: /jogo
*/
function get_JOGO($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 404 Not Found', true, 404);
    echo '{"erro": "Informe o ID do Jogo"}';
    return false;
  }

  $game = $sistema->getGame($URI[1]);

  if($game == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  echo json_encode($game);

  return true;
}

/*
VERBO: GET
ENDPOINT: /user
*/
function get_USER($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 404 Not Found', true, 404);
    echo '{"erro": "Informe o ID do USER"}';
    return false;
  }

  $user = $sistema->getUser($URI[1]);

  if($user == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  echo json_encode($user);

  return true;
}

/*
VERBO: GET
ENDPOINT: /userByName
*/
function get_USERByName($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 400 Bad Request', true, 400);
    echo '{"erro": "Informe o login do USER"}';
    return false;
  }

  $user = $sistema->getUserByName($URI[1]);

  if($user == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  echo json_encode($user);

  return true;
}

/*
VERBO: GET
ENDPOINT: /vods
*/
function get_VODS($URI) {
  global $sistema;

  if(!array_key_exists("pagina", $_GET) && ( !array_key_exists("inicio", $_GET) || !array_key_exists("fim", $_GET) )) {
    header('HTTP/1.1 400 Bad Request', true, 400);
    echo '{"erro": "Informe inicio e fim na consulta ou a página"}';
    return false;
  }

  $manterMomentos = false;
  if(array_key_exists("manterMomentos", $_GET)) $manterMomentos = true;

  if(!array_key_exists("idUser", $_GET))
    echo json_encode( $sistema->getVODsDoPeriodo((int)$_GET['inicio'], (int)$_GET['fim'], $manterMomentos) );
  elseif(array_key_exists("pagina", $_GET))
    echo json_encode( $sistema->getVODsDoStreamerPorPagina($_GET['idUser'], (int)$_GET['pagina'], $manterMomentos) );
  else
    echo json_encode( $sistema->getVODsDoStreamer($_GET['idUser'], (int)$_GET['inicio'], (int)$_GET['fim'], $manterMomentos) );

  return true;
}

/*
VERBO: GET
ENDPOINT: /estatisticas
*/
function get_Estatisticas($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 404 Not Found', true, 404);
    echo '{"erro": "Informe o tipo de estatistica"}';
    return false;
  }

  switch ($URI[1]) {
    case 'maisjogados':
      echo json_encode($sistema->maisJogadosSemana());
      break;
    case 'streamersemascensao':
      echo json_encode($sistema->streamersEmAscensao());
      break;
    case 'onlineagora':
      echo json_encode($sistema->getStreamersOnline());
      break;

    default:
      header('HTTP/1.1 400 Bad Request', true, 400);
      echo '{"erro": "Estatistica não suportada"}';
      return false;
      break;
  }

  return true;
}

/*
VERBO: GET
ENDPOINT: /globalTwitchBadges
*/
function get_GlobalTwitchBadges() {
  global $sistema;

  $r = $sistema->getGlobalTwitchBadges();

  if($r == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  echo $r;

  return true;
}

  /*
  VERBO: GET
  ENDPOINT: /channelTwitchBadges
  */
  function get_SearchUser($URI) {
    global $sistema;

    if(count($URI) != 2 || $URI[1] == '') {
      header('HTTP/1.1 404 Not Found', true, 404);
      echo '{"erro": "Informe o nome"}';
      return false;
    }

    $r = $sistema->searchUser($URI[1]);

    if(gettype($r) !== 'array' || count($r) === 0) {
      header('HTTP/1.1 404 Not Found', true, 404);
      echo '{"erro": "Nome não encontrado"}';
      return false;
    }

    echo json_encode($r);
    return true;
  }

/*
VERBO: GET
ENDPOINT: /channelTwitchBadges
*/
function get_ChannelTwitchBadges($URI) {
  global $sistema;

  if(count($URI) != 2 || $URI[1] == '') {
    header('HTTP/1.1 404 Not Found', true, 404);
    echo '{"erro": "Informe o id do canal"}';
    return false;
  }

  $r = $sistema->getChannelTwitchBadges($URI[1]);

  if($r == null) {
    header('HTTP/1.1 404 Not Found', true, 404);
    return false;
  }

  echo $r;

  return true;
}

?>
